package ba.unsa.etf.rma.edihanjalic.eh_17726;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by EH14 on 14-Apr-18.
 */

public class Autor implements Parcelable {
    protected Autor(Parcel in) {
        imeiPrezime = in.readString();
        brojKnjiga = in.readInt();
    }

    public static final Parcelable.Creator<Autor> CREATOR = new Creator<Autor>() {
        @Override
        public Autor createFromParcel(Parcel in) {
            return new Autor(in);
        }

        @Override
        public Autor[] newArray(int size) {
            return new Autor[size];
        }
    };

    public String getImeprezime() {
        return imeiPrezime;
    }

    public void setImeprezime(String imeprezime) {
        this.imeiPrezime = imeprezime;
    }

    public int getBrojKnjiga() {
        return brojKnjiga;
    }

    public void setBrojKnjiga(int brojKnjiga) {
        this.brojKnjiga = brojKnjiga;
    }

    public Autor(String imeiPrezime, ArrayList<String> knjige) {
        this.imeiPrezime = imeiPrezime;
        this.knjige = knjige;
    }

    private String imeiPrezime;
    private ArrayList<String> knjige;

    public String getImeiPrezime() {
        return imeiPrezime;
    }

    public void setImeiPrezime(String imeiPrezime) {
        this.imeiPrezime = imeiPrezime;
    }

    public ArrayList<String> getKnjige() {
        return knjige;
    }

    public void setKnjige(ArrayList<String> knjige) {
        this.knjige = knjige;
    }

    private int brojKnjiga;

    public Autor(String imeprezime, int brojKnjiga) {
        this.imeiPrezime = imeprezime;
        this.brojKnjiga = brojKnjiga;
    }

    public Autor(String imeprezime, String idKnjige) {
        this.imeiPrezime = imeprezime;
        this.knjige=new ArrayList<String>();
        knjige.add(idKnjige);
    }

    public void dodajKnjigu(String idKnjige){
        if(!this.knjige.contains(idKnjige)) this.knjige.add(idKnjige);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(imeiPrezime);
        parcel.writeInt(brojKnjiga);
    }
}

