package ba.unsa.etf.rma.edihanjalic.eh_17726;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import static ba.unsa.etf.rma.edihanjalic.eh_17726.DodavanjeKnjigeAkt.knjige;
import static ba.unsa.etf.rma.edihanjalic.eh_17726.KategorijeAkt.autori;
import static ba.unsa.etf.rma.edihanjalic.eh_17726.KategorijeAkt.kategorije;

/**
 * Created by EH14 on 11-Apr-18.
 */

public class ListeFragment extends Fragment {

    ArrayAdapter adapter;
    int prikazkategorije=1;

   @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
       View v = inflater.inflate(R.layout.listefragment, container, false);
       return v;
   }

    public void onActivityCreated(@Nullable Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

       final Button dDodajKategoriju = (Button)getView().findViewById(R.id.dDodajKategoriju);
       final ListView listaKategorija = (ListView)getView().findViewById(R.id.listaKategorija);
       final Button dPretraga = (Button)getView().findViewById(R.id.dPretraga);
       final EditText pretraga = (EditText) getView().findViewById(R.id.tekstPretraga);
       final Button dKategorije=getView().findViewById(R.id.dKategorije);
       final Button dAutori =getView().findViewById(R.id.dAutori);
     /*   kategorije.add(0,"Roman");
        kategorije.add(0,"Novela");
        kategorije.add(0,"Ljetopis");
        kategorije.add(0,"Putopis");
        kategorije.add(0,"Hronika");
        kategorije.add(0,"Nauka");
        kategorije.add(0,"Enciklopedija");
        kategorije.add(0,"Sport");  */
       adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, kategorije);
       listaKategorija.setAdapter(adapter);

       final Button dDodajOnline = (Button) getView().findViewById(R.id.dDodajOnline);
       dDodajOnline.setOnClickListener(new View.OnClickListener(){
           @Override
           public void onClick(View view) {
               FragmentOnline fo = new FragmentOnline();
               FragmentManager fm = getFragmentManager();
               fo = (FragmentOnline) fm.findFragmentByTag("FragmentOnline");

               if(fo == null){
                   fo = new FragmentOnline();
                   fm.beginTransaction().replace(R.id.mjestoF1, fo, "FragmentOnline").commit();
               }
               else {
                   fm.beginTransaction().replace(R.id.mjestoF1, fo, "FragmentOnline").commit();
               }
           }
       });

       final Button dDodajKnjigu = (Button)getView().findViewById(R.id.dDodajKnjigu);
       dDodajKnjigu.setOnClickListener(new View.OnClickListener(){
           @Override
           public void onClick(View view) {
               DodavanjeKnjigeFragment dkf = new DodavanjeKnjigeFragment();
               FragmentManager fm = getFragmentManager();
               Bundle b = new Bundle();
               b.putStringArrayList("kategorija1", kategorije);
               dkf.setArguments(b);
               fm.beginTransaction().replace(R.id.mjestoF1,dkf).addToBackStack(null).commit();
           }
       });

       dPretraga.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               (ListeFragment.this).adapter.getFilter().filter(pretraga.getText().toString(), new Filter.FilterListener() {
                   @Override
                   public void onFilterComplete(int count) {
                       if (count == 0) {
                           dDodajKategoriju.setEnabled(true);
                           dDodajKategoriju.setOnClickListener(new View.OnClickListener() {
                               @Override
                               public void onClick(View view) {
                                   kategorije.add(0, pretraga.getText().toString());
                                   pretraga.setText("");
                                   adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, kategorije);
                                   listaKategorija.setAdapter(adapter);

                               }
                           });
                       } else {
                           dDodajKategoriju.setEnabled(false);
                       }
                   }
               });
           }
       });

       listaKategorija.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
               if(prikazkategorije==0) {
              //     autori = getArguments().getParcelableArrayList("autori");
                   FragmentManager fm = getFragmentManager();
                   KnjigeFragment kf = new KnjigeFragment();
                   Bundle b = new Bundle();
                   b.putParcelable("aut", autori.get(position));
                   kf.setArguments(b);
                   fm.beginTransaction().replace(R.id.mjestoF1, kf).addToBackStack(null).commit();
               }
               else {
//                   kategorije = getArguments().getStringArrayList("kategorije");
                   FragmentManager fm = getFragmentManager();
                   KnjigeFragment kf = new KnjigeFragment();
                   Bundle b = new Bundle();
                   b.putString("kat", kategorije.get(position));
                   kf.setArguments(b);
                   fm.beginTransaction().replace(R.id.mjestoF1, kf).addToBackStack(null).commit();

               }
           }
       });

       dKategorije.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prikazkategorije=1;
                adapter=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,kategorije);
                listaKategorija.setAdapter(adapter);
                dPretraga.setVisibility(View.VISIBLE);
                dDodajKategoriju.setVisibility(View.VISIBLE);
                pretraga.setVisibility(View.VISIBLE);

            }
        });

       dAutori.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               prikazkategorije=0;
               autori.clear();
               for (Knjiga k:knjige) {
                   Boolean ima=false;
                   for(Autor a:autori){
                       if(k.getImeAutora().equals(a.getImeprezime())){
                           ima=true; break;
                       }
                   }
                   if(!ima){int brknjiga=0;
                       for(Knjiga k1:knjige){if(k1.getImeAutora().equals(k.getImeAutora())){brknjiga++;}}
                       autori.add(new Autor(k.getImeAutora(),brknjiga));}
               }
               final AdapterZaAutora adapterautor= new AdapterZaAutora(getActivity(),autori);
               listaKategorija.setAdapter(adapterautor);
               dPretraga.setVisibility(View.GONE);
               dDodajKategoriju.setVisibility(View.GONE);
               pretraga.setVisibility(View.GONE);
           }
       });



   }




}
