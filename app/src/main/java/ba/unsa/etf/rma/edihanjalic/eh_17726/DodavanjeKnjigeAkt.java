package ba.unsa.etf.rma.edihanjalic.eh_17726;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DodavanjeKnjigeAkt extends AppCompatActivity {
    private static int RESULT_LOAD_IMAGE = 1;

    public static ArrayList<Knjiga> knjige = new ArrayList<Knjiga>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodavanje_knjige_akt);
        //Intent intent = getIntent();
       /* final Spinner sKategorijaKnjige = (Spinner)findViewById(R.id.sKategorijaKnjige);

        ArrayAdapter adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, KategorijeAkt.kategorije);
        sKategorijaKnjige.setAdapter(adapter);
        Button dUpisiKnjigu = (Button)findViewById(R.id.dUpisiKnjigu);
        final EditText imeAutora = (EditText)findViewById(R.id.imeAutora);
        final EditText nazivKnjige = (EditText)findViewById(R.id.nazivKnjige);
        Button dPonisti = (Button)findViewById(R.id.dPonisti);

        dPonisti.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent vracanjenapocetnu = new Intent(DodavanjeKnjigeAkt.this, KategorijeAkt.class);
                startActivity(vracanjenapocetnu);
            }
        });

        dUpisiKnjigu.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Knjiga temp = new Knjiga();
                temp.setNazivKnjige(nazivKnjige.getText().toString());
                temp.setImeAutora(imeAutora.getText().toString());
                temp.setKategorija(sKategorijaKnjige.getSelectedItem().toString());
                knjige.add(temp);
                Intent vracanjenapocetnu = new Intent(DodavanjeKnjigeAkt.this, KategorijeAkt.class);
                startActivity(vracanjenapocetnu);
            }
        });

        Button dNadjiSliku = (Button)findViewById(R.id.dNadjiSliku);
        dNadjiSliku.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                if(intent.resolveActivity(getPackageManager()) != null){
                    startActivityForResult(intent,RESULT_LOAD_IMAGE);
                }
            }
        }); */



    }
/*
    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    protected void onActivityResult(int requestcode, int resultcode, Intent data){
        if(resultcode == RESULT_CANCELED){}
        if(resultcode == RESULT_OK){
            EditText nazivKnjige = (EditText) findViewById(R.id.nazivKnjige);
            Uri odabranaSlika = data.getData();
            try{
                FileOutputStream os;
                os = openFileOutput(nazivKnjige.getText().toString(), Context.MODE_PRIVATE);
                try{
                    getBitmapFromUri(data.getData()).compress(Bitmap.CompressFormat.JPEG, 90, os);
                    os.close();
                } catch (IOException exxx){
                    Logger.getLogger(DodavanjeKnjigeAkt.class.getName()).log(Level.SEVERE, null, exxx);
                }
                ImageView slika = (ImageView)findViewById(R.id.eNaslovna);
                slika.setImageBitmap(BitmapFactory.decodeStream(openFileInput(nazivKnjige.getText().toString())));
            } catch (FileNotFoundException exx){
                    Logger.getLogger(DodavanjeKnjigeAkt.class.getName()).log(Level.SEVERE, null, exx);
            }
        }

    } */
}
