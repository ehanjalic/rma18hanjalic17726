package ba.unsa.etf.rma.edihanjalic.eh_17726;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static ba.unsa.etf.rma.edihanjalic.eh_17726.DodavanjeKnjigeAkt.knjige;

/**
 * Created by EH14 on 30-Mar-18.
 */

public class AdapterZaKnjigu extends BaseAdapter {

    private ArrayList<Knjiga> singleRow;
    private LayoutInflater thisInflater;
    private Context context;

    public AdapterZaKnjigu(Context context, ArrayList<Knjiga> aRow) {

        this.singleRow = aRow;
        thisInflater = ( LayoutInflater.from(context) );

    }


    @Override
    public int getCount() {
        return singleRow.size( );
    }

    @Override
    public Object getItem(int pozicija) {
        return singleRow.get( pozicija );
    }

    @Override
    public long getItemId(int pozicija) {
        return pozicija;
    }

    @Override
    public View getView(int pozicija, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = thisInflater.inflate( R.layout.element_liste, parent, false );
            TextView nazivKnjige = (TextView) convertView.findViewById(R.id.eNaziv);
            TextView imeAutora = (TextView) convertView.findViewById(R.id.eAutor);
            ImageView theImage = (ImageView) convertView.findViewById(R.id.eNaslovna);
            TextView datumObjavljivanja = (TextView) convertView.findViewById(R.id.eDatumObjavljivanja);
            TextView opis = (TextView) convertView.findViewById(R.id.eOpis);
            TextView brojStranica = (TextView) convertView.findViewById(R.id.eBrojStranica);


            final Knjiga currentRow = (Knjiga) getItem(pozicija);

            nazivKnjige.setText( currentRow.getNazivKnjige() );
            /*imeAutora.setText("");
            for(int i = 0; i<currentRow.getAutori().size(); i++){
                imeAutora.append(currentRow.getAutori().get(i)+" ");
            }*/
            ArrayList<Autor> auts = currentRow.getAutori();
            String autori = "";
            for(int i = 0; i<auts.size(); i++){
                autori = autori + auts.get(i).getImeiPrezime() + " ";
            }
            imeAutora.setText(autori);

            //imeAutora.setText( currentRow.getImeAutora() );
            datumObjavljivanja.setText(currentRow.getDatumObjavljivanja());
            opis.setText(currentRow.getOpis());
            int brs = currentRow.getBrojStranica();
            if(brs != 0) {
                brojStranica.setText((String.valueOf((brs))));
            }
            else {
                brojStranica.setText("Nepoznat");
            }
            URL url = currentRow.getSlikaa();
            Picasso.get().load(url.toString()).into(theImage);

            if(currentRow.getBoja()) convertView.setBackgroundColor(0xffaabbed);
            Button dPreporuci = (Button)convertView.findViewById(R.id.dPreporuci);
            dPreporuci.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    context = view.getContext();
                    FragmentPreporuci fp = new FragmentPreporuci();
                    FragmentManager fm = ((Activity)context).getFragmentManager();
                    Bundle b = new Bundle();
                    b.putParcelable("knjiga1", currentRow);
                    fp.setArguments(b);
                    fm.beginTransaction().replace(R.id.mjestoF1,fp).addToBackStack(null).commit();
                }
            });
        }


        return convertView;
    }
}
