package ba.unsa.etf.rma.edihanjalic.eh_17726;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;

import static ba.unsa.etf.rma.edihanjalic.eh_17726.DodavanjeKnjigeAkt.knjige;

/**
 * Created by EH14 on 30-May-18.
 */



public class FragmentPreporuci extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragmentpreporuci, container, false);

        return v;
    }

    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        TextView nazivKnjige = (TextView) getView().findViewById(R.id.eNaziv2);
        TextView imeAutora = (TextView) getView().findViewById(R.id.eAutor2);
        ImageView theImage = (ImageView) getView().findViewById(R.id.eNaslovna2);
        TextView datumObjavljivanja = (TextView) getView().findViewById(R.id.eDatumObjavljivanja2);
        TextView opis = (TextView) getView().findViewById(R.id.eOpis2);
        TextView brojStranica = (TextView) getView().findViewById(R.id.eBrojStranica2);

        Bundle b = getArguments();
        Knjiga currentRow = new Knjiga();
        if(b != null){
            if(b.containsKey("knjiga1")){
                currentRow = getArguments().getParcelable("knjiga1");
            }
        }


        nazivKnjige.setText( currentRow.getNazivKnjige() );
            /*imeAutora.setText("");
            for(int i = 0; i<currentRow.getAutori().size(); i++){
                imeAutora.append(currentRow.getAutori().get(i)+" ");
            }*/
        ArrayList<Autor> auts = currentRow.getAutori();
        String autori = "";
        for(int i = 0; i<auts.size(); i++){
            autori = autori + auts.get(i).getImeiPrezime() + " ";
        }
        imeAutora.setText(autori);

        //imeAutora.setText( currentRow.getImeAutora() );
        datumObjavljivanja.setText(currentRow.getDatumObjavljivanja());
        opis.setText(currentRow.getOpis());
        int brs = currentRow.getBrojStranica();
        if(brs != 0) {
            brojStranica.setText((String.valueOf((brs))));
        }
        else {
            brojStranica.setText("Nepoznat");
        }
        URL url = currentRow.getSlikaa();
        Picasso.get().load(url.toString()).into(theImage);

        final Spinner sKontakti = (Spinner)getView().findViewById(R.id.sKontakti);
        final ArrayList<String> imena = getNameDetails();
        final ArrayList<String> mailovi = getNameEmailDetails();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, imena);
        sKontakti.setAdapter(adapter);

        final String ime_knjige = currentRow.getNazivKnjige();
        final String ime_prvog_autora;
        if(auts.get(0).getImeiPrezime() != null) {
            ime_prvog_autora = auts.get(0).getImeiPrezime();
        }
        else ime_prvog_autora = "Nepoznat autor";

        Button dPosalji = (Button)getView().findViewById(R.id.dPosalji);
        dPosalji.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                String ime_mail = sKontakti.getSelectedItem().toString();
                String mail = "";
                for (int i = 0; i<imena.size(); i++){
                    if(imena.get(i).equals(ime_mail)){
                        mail = mailovi.get(i);
                        break;
                    }
                }
                String poruka;
                poruka = "Zdravo " + ime_mail + "\nProcitaj knjigu " + ime_knjige +" od " + ime_prvog_autora + "!";
                sendEmail(mail, poruka);


                ListeFragment lf = new ListeFragment();
                FragmentManager fm = getFragmentManager();
                Bundle b = new Bundle();
                b.putParcelableArrayList("knjiga1", knjige);
                lf.setArguments(b);
                fm.beginTransaction().replace(R.id.mjestoF1,lf).addToBackStack(null).commit();
            }
        });



    }

    protected void sendEmail(String mail, String poruka) {
        Log.i("Send email", "");
        String[] TO = {mail};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);

        emailIntent.putExtra(Intent.EXTRA_TEXT, poruka);
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            getActivity().finish();
            Log.i("Finished sending email", "");
        }
        catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getActivity(), "There is no email client installed.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public ArrayList<String> getNameEmailDetails() {
        ArrayList<String> emlRecs = new ArrayList<String>();
        HashSet<String> emlRecsHS = new HashSet<String>();
        Context context = getActivity();
        ContentResolver cr = context.getContentResolver();
        String[] PROJECTION = new String[] { ContactsContract.RawContacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.Contacts.PHOTO_ID,
                ContactsContract.CommonDataKinds.Email.DATA,
                ContactsContract.CommonDataKinds.Photo.CONTACT_ID };
        String order = "CASE WHEN "
                + ContactsContract.Contacts.DISPLAY_NAME
                + " NOT LIKE '%@%' THEN 1 ELSE 2 END, "
                + ContactsContract.Contacts.DISPLAY_NAME
                + ", "
                + ContactsContract.CommonDataKinds.Email.DATA
                + " COLLATE NOCASE";
        String filter = ContactsContract.CommonDataKinds.Email.DATA + " NOT LIKE ''";
        Cursor cur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, PROJECTION, filter, null, order);
        Log.d("__",Integer.toString(cur.getCount()));
        if (cur.moveToFirst()) {
            do {
                // names comes in hand sometimes
                String name = cur.getString(1);
                String emlAddr = cur.getString(3);

                // keep unique only
                if (emlRecsHS.add(emlAddr.toLowerCase())) {
                    emlRecs.add(emlAddr);
                }
            } while (cur.moveToNext());
        }

        cur.close();
        return emlRecs;
    }

    public ArrayList<String> getNameDetails() {
        ArrayList<String> emlRecs = new ArrayList<String>();
        HashSet<String> emlRecsHS = new HashSet<String>();
        Context context = getActivity();
        ContentResolver cr = context.getContentResolver();
        String[] PROJECTION = new String[] { ContactsContract.RawContacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.Contacts.PHOTO_ID,
                ContactsContract.CommonDataKinds.Email.DATA,
                ContactsContract.CommonDataKinds.Photo.CONTACT_ID };
        String order = "CASE WHEN "
                + ContactsContract.Contacts.DISPLAY_NAME
                + " NOT LIKE '%@%' THEN 1 ELSE 2 END, "
                + ContactsContract.Contacts.DISPLAY_NAME
                + ", "
                + ContactsContract.CommonDataKinds.Email.DATA
                + " COLLATE NOCASE";
        String filter = ContactsContract.CommonDataKinds.Email.DATA + " NOT LIKE ''";
        Cursor cur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, PROJECTION, filter, null, order);
        Log.d("__",Integer.toString(cur.getCount()));
        if (cur.moveToFirst()) {
            do {
                // names comes in hand sometimes
                String name = cur.getString(1);
                String emlAddr = cur.getString(3);

                // keep unique only
                if (emlRecsHS.add(name.toLowerCase())) {
                    emlRecs.add(name);
                }
            } while (cur.moveToNext());
        }

        cur.close();
        return emlRecs;
    }


}


