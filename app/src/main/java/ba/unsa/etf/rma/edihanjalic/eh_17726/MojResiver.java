package ba.unsa.etf.rma.edihanjalic.eh_17726;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

/**
 * Created by EH14 on 21-May-18.
 */

public class MojResiver extends ResultReceiver {
    private Receiver mReceiver;

    public MojResiver(Handler handler){
        super(handler);
    }

    public  void setReceiver (Receiver receiver){
        mReceiver = receiver;
    }

    public interface Receiver{
        public void onReceiveResult(int resultCode, Bundle resultData);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData){
        if(mReceiver != null){
            mReceiver.onReceiveResult(resultCode, resultData);
        }
    }
}
