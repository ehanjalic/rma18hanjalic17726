package ba.unsa.etf.rma.edihanjalic.eh_17726;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;


/**
 * Created by EH14 on 21-May-18.
 */

public class KnjigePoznanika extends IntentService {

    final String API_KEY = "AIzaSyCWsmbRBqcj1ycgw2Ki96PT0yx4OBMKBro";
    final public static int STATUS_RUNNING=0;
    final public static int STATUS_FINISHED=1;
    final public static int STATUS_ERROR=2;
    ArrayList<Knjiga> rez = new ArrayList<Knjiga>();
    public KnjigePoznanika() {
        super(null);
    }

    public KnjigePoznanika(String ime){
        super(ime);
    }

    @Override
    public void onCreate(){
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent){
        final ResultReceiver receiver = intent.getParcelableExtra("receiver");
        Bundle bundle = new Bundle();
        receiver.send(STATUS_RUNNING, Bundle.EMPTY);
        String query = null;
        try{
            String userID = intent.getStringExtra("idKorisnika");
            query = URLEncoder.encode(userID, "utf-8");
        } catch(UnsupportedEncodingException e) {
            e.printStackTrace();
            receiver.send(STATUS_ERROR, Bundle.EMPTY);
        }
        String url1 = "https://www.googleapis.com/books/v1/users/"+ query + "/bookshelves?key=" + API_KEY;
        try{
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertStreamToString(in);
            JSONObject jo = new JSONObject(rezultat);
            JSONArray knjige = jo.getJSONArray("items");
            for(int i =0; i<knjige.length(); i++){

                String naziv, datumObjavljivanja, opis, urlslike, brojStranicaS, id;
                id="";
                naziv="";
                datumObjavljivanja="";
                opis="Knjiga nema opis.";
                urlslike="";
                brojStranicaS="";
                JSONObject knjiga = knjige.getJSONObject(i);
                //JSONObject volumeInfo = knjiga.getJSONObject("volumeInfo");
                if(knjiga.has("id")){
                    id=knjiga.getString("id");
                }
                if(knjiga.has("title")) {
                    naziv = knjiga.getString("title");
                }
                JSONArray autori = new JSONArray();
                try {
                    autori = knjiga.getJSONArray("authors");
                    if (knjiga.has("publishedDate")) {
                        datumObjavljivanja = knjiga.getString("publishedDate");
                    }
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
                if(knjiga.has("description")) {
                    opis = knjiga.getString("description");
                }
               /* JSONObject slike = volumeInfo.getJSONObject("imageLinks");

                if(slike.has("thumbnail")) {
                    urlslike = slike.getString("thumbnail");
                }
                URL slika = new URL(urlslike); */

                URL slika = new URL("http://image10.bizrate-images.com/resize?sq=60&uid=2216744464");

                if (knjiga.has("imageLinks")) {
                    JSONObject imageLinks = knjiga.getJSONObject("imageLinks");

                    if (imageLinks.has("thumbnail")) {
                        String slikica = imageLinks.getString("thumbnail");
                        slika = new URL(slikica);
                    }
                }


                int brojStranica = 0;
                if(knjiga.has("pageCount")) {
                    brojStranicaS = knjiga.getString("pageCount");
                    brojStranica = Integer.parseInt(brojStranicaS);
                }
                ArrayList<Autor> listaAutora = new ArrayList<Autor>();
                for(int j = 0; j<autori.length(); j++){
                    listaAutora.add(new Autor(autori.getString(j), id));
                }
                rez.add(new Knjiga(id,naziv, listaAutora, datumObjavljivanja, opis,  brojStranica, slika ));
            }
        } catch (MalformedURLException e){
            e.printStackTrace();
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        bundle.putParcelableArrayList("result", rez);
        receiver.send(STATUS_FINISHED, bundle);

    }

    public String convertStreamToString(InputStream is){
        BufferedReader reader = new BufferedReader( new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try{
            while((line = reader.readLine()) != null){
                sb.append(line + "\n");
            }
        } catch (IOException e){
        } finally {
            try {
                is.close();
            } catch (IOException e){
            }
        }
        return sb.toString();
    }
}
