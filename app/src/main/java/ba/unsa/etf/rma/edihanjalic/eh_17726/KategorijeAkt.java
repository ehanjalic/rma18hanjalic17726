package ba.unsa.etf.rma.edihanjalic.eh_17726;

import android.Manifest;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class KategorijeAkt extends AppCompatActivity {

    ArrayAdapter adapter;
    public static ArrayList<String> kategorije = new ArrayList<String>();
    public static ArrayList<Autor> autori = new ArrayList<Autor>();
    public static final int REQUEST_READ_CONTACTS=79;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Intent intent = getIntent();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kategorije_akt);


        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)!= PackageManager.PERMISSION_GRANTED){
            if(!(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CONTACTS))){
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_CONTACTS},REQUEST_READ_CONTACTS);
            }
        }


       /* final Button dDodajOnline = (Button)findViewById(R.id.dDodajOnline);
        dDodajOnline.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                FragmentManager fm = getFragmentManager();
                FragmentOnline fo = (FragmentOnline) fm.findFragmentByTag("FragmentOnline");

                if(fo == null){
                    fo = new FragmentOnline();
                    fm.beginTransaction().replace(R.id.mjestoF1, fo, "FragmentOnline").commit();
                }
                else {

                }
            }
        }); */

       /* final Button dDodajKategoriju = (Button)findViewById(R.id.dDodajKategoriju);
        final ListView listaKategorija = (ListView)findViewById(R.id.listaKategorija);
        final Button dPretraga = (Button)findViewById(R.id.dPretraga);
        kategorije.add(0,"Roman");
        kategorije.add(0,"Novela");
        kategorije.add(0,"Ljetopis");
        kategorije.add(0,"Putopis");
        kategorije.add(0,"Hronika");
        kategorije.add(0,"Nauka");
        kategorije.add(0,"Enciklopedija");
        kategorije.add(0,"Sport");
        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, kategorije);
        listaKategorija.setAdapter(adapter);

        final Button dDodajKnjigu = (Button)findViewById(R.id.dDodajKnjigu);
        dDodajKnjigu.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(KategorijeAkt.this, DodavanjeKnjigeAkt.class);
                startActivity(intent);
            }
        });



        dPretraga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText pretraga = (EditText) findViewById(R.id.tekstPretraga);

                (KategorijeAkt.this).adapter.getFilter().filter(pretraga.getText().toString(), new Filter.FilterListener() {
                    @Override
                    public void onFilterComplete(int count) {
                        if (count == 0) {
                            dDodajKategoriju.setEnabled(true);
                            dDodajKategoriju.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    kategorije.add(0, pretraga.getText().toString());
                                    pretraga.setText("");
                                    adapter = new ArrayAdapter<String>(KategorijeAkt.this, android.R.layout.simple_list_item_1, kategorije);
                                    listaKategorija.setAdapter(adapter);

                                }
                            });
                        } else {
                            dDodajKategoriju.setEnabled(false);
                        }
                    }
                });
            }
        });

       listaKategorija.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent idiNaListu = new Intent(KategorijeAkt.this, ListaKnjigaAkt.class);
                String kategorija = adapter.getItem(position).toString();
                idiNaListu.putExtra("kategorija", kategorija);
                startActivity(idiNaListu);
            }
        });

*/


        FragmentManager fm = getFragmentManager();
        ListeFragment lf = (ListeFragment) fm.findFragmentByTag("Liste");

        if(lf == null){
            lf = new ListeFragment();
            fm.beginTransaction().replace(R.id.mjestoF1, lf, "Lista").commit();
        }
        else {

        }



}



}
