package ba.unsa.etf.rma.edihanjalic.eh_17726;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by EH14 on 20-May-18.
 */

public class DohvatiNajnovije extends AsyncTask<String, Integer, Void> {
    public interface iDohvatiNajnovijeDone{
        public void onNajnovijeDone(ArrayList<Knjiga> rez);
    }
    ArrayList<Knjiga> rez;
    private DohvatiNajnovije.iDohvatiNajnovijeDone pozivatelj;
    public DohvatiNajnovije(DohvatiNajnovije.iDohvatiNajnovijeDone p){pozivatelj = p;};


    @Override
    protected Void doInBackground(String... params){
        String query = null;
        rez=new ArrayList<>();
        try{
            query = URLEncoder.encode(params[0], "utf-8");
        }
        catch(UnsupportedEncodingException e){
            e.printStackTrace();
        }
        String url1 = "https://www.googleapis.com/books/v1/volumes?q=inauthor:"+query+"&orderBy=newest&maxResults=5";
        try{
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertStreamToString(in);
            JSONObject jo = new JSONObject(rezultat);
            JSONArray knjige = jo.getJSONArray("items");
            for(int i =0; i<knjige.length(); i++){

                String naziv, datumObjavljivanja, opis, urlslike, brojStranicaS, id;
                id="";
                naziv="";
                datumObjavljivanja="";
                opis="Knjiga nema opis.";
                urlslike="";
                brojStranicaS="";
                JSONObject knjiga = knjige.getJSONObject(i);
                JSONObject volumeInfo = knjiga.getJSONObject("volumeInfo");
                if(knjiga.has("id")){
                    id=knjiga.getString("id");
                }
                if(volumeInfo.has("title")) {
                    naziv = volumeInfo.getString("title");
                }
                JSONArray autori = new JSONArray();
                try {
                    autori = volumeInfo.getJSONArray("authors");
                    if (volumeInfo.has("publishedDate")) {
                        datumObjavljivanja = volumeInfo.getString("publishedDate");
                    }
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
                if(volumeInfo.has("description")) {
                    opis = volumeInfo.getString("description");
                }
            /*JSONObject slike = volumeInfo.getJSONObject("imageLinks");

            if(slike.has("thumbnail")) {
                urlslike = slike.getString("thumbnail");
            }
            URL slika = new URL(urlslike); */

                URL slika = new URL("http://image10.bizrate-images.com/resize?sq=60&uid=2216744464");

                if (volumeInfo.has("imageLinks")) {
                    JSONObject imageLinks = volumeInfo.getJSONObject("imageLinks");

                    if (imageLinks.has("thumbnail")) {
                        String slikica = imageLinks.getString("thumbnail");
                        slika = new URL(slikica);
                    }
                }


                int brojStranica = 0;
                if(volumeInfo.has("pageCount")) {
                    brojStranicaS = volumeInfo.getString("pageCount");
                    brojStranica = Integer.parseInt(brojStranicaS);
                }

                ArrayList<Autor> listaAutora = new ArrayList<Autor>();
                for(int j = 0; j<autori.length(); j++){
                    listaAutora.add(new Autor(autori.getString(j), id));
                }
                rez.add(new Knjiga(id,naziv, listaAutora, datumObjavljivanja, opis, brojStranica, slika ));
            }
        }
        catch (MalformedURLException e){
            e.printStackTrace();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        return null;
    }

    public String convertStreamToString(InputStream is){
        BufferedReader reader = new BufferedReader( new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try{
            while((line = reader.readLine()) != null){
                sb.append(line + "\n");
            }
        } catch (IOException e){
        } finally {
            try {
                is.close();
            } catch (IOException e){
            }
        }
        return sb.toString();
    }


    @Override
    protected void onPostExecute(Void aVoid){
        super.onPostExecute(aVoid);
        pozivatelj.onNajnovijeDone(rez);
    }
}
