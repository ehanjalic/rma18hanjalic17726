package ba.unsa.etf.rma.edihanjalic.eh_17726;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;

import static ba.unsa.etf.rma.edihanjalic.eh_17726.DodavanjeKnjigeAkt.knjige;

/**
 * Created by EH14 on 14-Apr-18.
 */

public class KnjigeFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.knjigefragment, container, false);

        Button dPovratak = (Button)v.findViewById(R.id.dPovratak);
        dPovratak.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                ListeFragment lf = new ListeFragment();
                FragmentManager fm = getFragmentManager();
                Bundle b = new Bundle();
                b.putParcelableArrayList("knjiga1", knjige);
                lf.setArguments(b);
                fm.beginTransaction().replace(R.id.mjestoF1,lf).addToBackStack(null).commit();
            }
        });

        final ArrayList<Knjiga> knjigeIzKategorije = new ArrayList<Knjiga>();
        ListView listaKnjiga = (ListView)v.findViewById(R.id.listaKnjiga);
        listaKnjiga.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                view.setBackgroundColor(0xffaabbed);
                for(Knjiga k : DodavanjeKnjigeAkt.knjige) {
                    if(k.getNazivKnjige() .equals(knjigeIzKategorije.get(position).getNazivKnjige())) {
                        k.setBoja();
                    }
                }
            }
        });


        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        String kategorijaa = null;
        Autor autorr = null;
        if(getArguments().containsKey("kat")){
            kategorijaa = getArguments().getString("kat");
            final ArrayList<Knjiga> knjigeIzKategorije = new ArrayList<Knjiga>();
            for(Knjiga k : DodavanjeKnjigeAkt.knjige){
                if(k.getKategorija().equals(kategorijaa)){
                    knjigeIzKategorije.add(k);
                }
            }
            ListView listaKnjiga = (ListView)getView().findViewById(R.id.listaKnjiga);
            final AdapterZaKnjigu adapter = new AdapterZaKnjigu((getActivity()), knjigeIzKategorije);
            listaKnjiga.setAdapter(adapter);
        }
        else if(getArguments().containsKey("aut")){
            autorr = getArguments().getParcelable("aut");
            final ArrayList<Knjiga> knjigeIzKategorije = new ArrayList<Knjiga>();
            for(Knjiga k : DodavanjeKnjigeAkt.knjige){
                if(k.getImeAutora().equals(autorr.getImeprezime())){
                    knjigeIzKategorije.add(0, k);
                }
            }
            ListView listaKnjiga = (ListView)getView().findViewById(R.id.listaKnjiga);
            final AdapterZaKnjigu adapter = new AdapterZaKnjigu((getActivity()), knjigeIzKategorije);
            listaKnjiga.setAdapter(adapter);

        }


    }


}
