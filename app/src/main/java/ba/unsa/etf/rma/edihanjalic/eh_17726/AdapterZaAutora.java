package ba.unsa.etf.rma.edihanjalic.eh_17726;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by EH14 on 14-Apr-18.
 */



public class AdapterZaAutora extends ArrayAdapter<Autor>{
    private Context mContext;
    private ArrayList<Knjiga> knjige;

    public AdapterZaAutora(Context context, ArrayList<Autor> k){super(context, 0, k);}

    @Override
    public View getView(int i, View view, ViewGroup viewGroup){
        if(view==null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.element_liste2, viewGroup, false);

            TextView imeprezime = (TextView) view.findViewById(R.id.eAutor);
            TextView brojknjiga = (TextView) view.findViewById(R.id.eBroj);

            imeprezime.setText(getItem(i).getImeprezime());
            brojknjiga.setText(getItem(i).getBrojKnjiga());
        }
        return view;
    }
}

