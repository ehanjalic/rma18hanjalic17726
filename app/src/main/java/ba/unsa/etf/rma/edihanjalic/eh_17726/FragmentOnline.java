package ba.unsa.etf.rma.edihanjalic.eh_17726;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import static ba.unsa.etf.rma.edihanjalic.eh_17726.DodavanjeKnjigeAkt.knjige;

/**
 * Created by EH14 on 20-May-18.
 */

public class FragmentOnline extends Fragment implements DohvatiKnjige.iDohvatiKnjigeDone, DohvatiNajnovije.iDohvatiNajnovijeDone, MojResiver.Receiver  {
    ArrayList<Knjiga> knjigice  = new ArrayList<Knjiga>();
    ArrayList<String> nazivi = new ArrayList<>();
    String tekstPretrage;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragmentonline, container, false);

        return v;
    }

    public void onActivityCreated(@Nullable Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        Button dPovratak = (Button)getView().findViewById(R.id.dPovratak);
        dPovratak.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                ListeFragment lf = new ListeFragment();
                FragmentManager fm = getFragmentManager();
                Bundle b = new Bundle();
                b.putParcelableArrayList("knjiga1", knjige);
                lf.setArguments(b);
                fm.beginTransaction().replace(R.id.mjestoF1,lf).addToBackStack(null).commit();
            }
        });




        final Spinner sKategorije = (Spinner) getView().findViewById(R.id.sKategorije);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, KategorijeAkt.kategorije);
        sKategorije.setAdapter(adapter);



        Button dRun = (Button)getView().findViewById(R.id.dRun);
        dRun.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                knjigice.clear();
                nazivi.clear();

                EditText TekstUpit = (EditText) getView().findViewById(R.id.tekstUpit);
                tekstPretrage = TekstUpit.getText().toString();

                if(tekstPretrage.contains(";")==false && tekstPretrage.contains("autor:")==false && tekstPretrage.contains("korisnik:")==false){
                    new DohvatiKnjige((DohvatiKnjige.iDohvatiKnjigeDone) FragmentOnline.this).execute(tekstPretrage);
                }
                else if(tekstPretrage.contains(";")==true){
                    String[] lista = tekstPretrage.split(";", 0);
                    for(String s : lista){
                        new DohvatiKnjige((DohvatiKnjige.iDohvatiKnjigeDone) FragmentOnline.this).execute(s);
                    }
                }
                else if(tekstPretrage.contains("autor:")){
                    String a;
                    a=tekstPretrage.substring(6);
                    new DohvatiNajnovije((DohvatiNajnovije.iDohvatiNajnovijeDone) FragmentOnline.this).execute(a);
                }
                else{
                    String korisnik = tekstPretrage.substring(9);
                    Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(), KnjigePoznanika.class);
                    MojResiver mReceiver = new MojResiver(new Handler());
                    mReceiver.setReceiver(FragmentOnline.this);
                    intent.putExtra("idKorisnika", korisnik);
                    intent.putExtra("receiver", mReceiver);
                    getActivity().startService(intent);
                }
            }
        });

        Button dAdd = (Button)getView().findViewById(R.id.dAdd);
        dAdd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Knjiga temp = new Knjiga();
                Spinner sRezultat = (Spinner) getView().findViewById(R.id.sRezultat);
                String ime_knjige = sRezultat.getSelectedItem().toString();
                for(Knjiga k : knjigice){
                    if(ime_knjige.equals(k.getNaziv())){
                        temp = k;
                        break;
                    }
                }
                temp.setKategorija(sKategorije.getSelectedItem().toString());
                knjige.add(temp);

                ListeFragment lf = new ListeFragment();
                FragmentManager fm = getFragmentManager();
                Bundle b = new Bundle();
                b.putParcelableArrayList("knjiga1", knjige);
                lf.setArguments(b);
                fm.beginTransaction().replace(R.id.mjestoF1,lf).addToBackStack(null).commit();
            }
        });

    }

    @Override
    public void onDohvatiDone (ArrayList<Knjiga> rez){
        knjigice = rez;
        for(Knjiga k : knjigice){
            nazivi.add(k.getNaziv());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, nazivi);
        Spinner sRezultat = (Spinner) getView().findViewById(R.id.sRezultat);
        sRezultat.setAdapter(adapter);

    }


    @Override
    public void onNajnovijeDone (ArrayList<Knjiga> rez){
        /*
        for(Knjiga k : rez){
            knjigice.add(k);
            nazivi.add(k.getNaziv());        }
*/
        knjigice = rez;
        for(Knjiga k : knjigice){
            nazivi.add(k.getNaziv());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, nazivi);
        Spinner sRezultat = (Spinner) getView().findViewById(R.id.sRezultat);
        sRezultat.setAdapter(adapter);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData){
        switch(resultCode){
            case KnjigePoznanika.STATUS_RUNNING:
                break;
            case KnjigePoznanika.STATUS_FINISHED:
                ArrayList<Knjiga> rez= resultData.getParcelableArrayList("result");
                knjigice = rez;
                for(Knjiga k : knjigice){
                    nazivi.add(k.getNaziv());
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, nazivi);
                Spinner sRezultat = (Spinner) getView().findViewById(R.id.sRezultat);
                sRezultat.setAdapter(adapter);
                break;
            case KnjigePoznanika.STATUS_ERROR:
                String error = resultData.getString(Intent.EXTRA_TEXT);
                Toast.makeText(getActivity(),error, Toast.LENGTH_LONG).show();
                break;
        }
    }
}
