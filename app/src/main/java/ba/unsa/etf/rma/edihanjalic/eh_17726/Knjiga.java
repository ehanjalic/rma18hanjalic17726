package ba.unsa.etf.rma.edihanjalic.eh_17726;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import java.net.URL;
import java.util.ArrayList;

/**
 * Created by EH14 on 30-Mar-18.
 */

public class Knjiga implements Parcelable {
    public String getIdKnjige() {
        return idKnjige;
    }

    public void setIdKnjige(String idKnjige) {
        this.idKnjige = idKnjige;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public ArrayList<Autor> getAutori() {
        return autori;
    }

    public void setAutori(ArrayList<Autor> autori) {
        this.autori = autori;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getDatumObjavljivanja() {
        return datumObjavljivanja;
    }

    public void setDatumObjavljivanja(String datumObjavljivanja) {
        this.datumObjavljivanja = datumObjavljivanja;
    }

    public void setSlika(URL slika) {
        this.slika = slika;
    }

    public URL getSlikaa(){
        return slika;
    }

    public int getBrojStranica() {
        return brojStranica;
    }

    public void setBrojStranica(int brojStranica) {
        this.brojStranica = brojStranica;
    }

    private String idKnjige;
    private String naziv;
    private ArrayList<Autor> autori;
    private String opis;
    private String datumObjavljivanja;
    private URL slika;
    private int brojStranica;

    public Knjiga(String idKnjige, String naziv, ArrayList<Autor> autori, String datumObjavljivanja, String opis,  int brojStranica, URL slika) {
        this.idKnjige = idKnjige;
        this.naziv = naziv;
        this.autori = autori;
        this.opis = opis;
        this.datumObjavljivanja = datumObjavljivanja;
        this.slika = slika;
        this.brojStranica = brojStranica;
    }

    private String imeAutora;
    private String kategorija;
    private int slika1;
    private boolean boja;


    public Knjiga(String nazivKnjige, String imeAutora, String kategorija, int slika) {
        this.naziv = nazivKnjige;
        this.imeAutora = imeAutora;
        this.kategorija = kategorija;
        this.slika1 = slika;
        this.boja = false;
    }

    public Knjiga() {
        this.boja = false;
    }

    protected Knjiga(Parcel in) {
        naziv = in.readString();
        imeAutora = in.readString();
        kategorija = in.readString();
        slika1 = in.readInt();
        boja = in.readByte() != 0;
    }

    public static final Creator<Knjiga> CREATOR = new Creator<Knjiga>() {
        @Override
        public Knjiga createFromParcel(Parcel in) {
            return new Knjiga(in);
        }

        @Override
        public Knjiga[] newArray(int size) {
            return new Knjiga[size];
        }
    };

    public String getNazivKnjige() {
        return naziv;
    }

    public void setNazivKnjige(String nazivKnjige) {
        this.naziv = nazivKnjige;
    }

    public String getImeAutora() {
        return imeAutora;
    }

    public void setImeAutora(String imeAutora) {
        this.imeAutora = imeAutora;
    }

    public String getKategorija() {
        return kategorija;
    }

    public void setKategorija(String kategorija) {
        this.kategorija = kategorija;
    }

    public int getSlika() {
        return slika1;
    }

    public void setSlika(int slika) {
        this.slika1 = slika;
    }

    public void setBoja(){
        this.boja = true;
    }

    public boolean getBoja(){return boja;}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(naziv);
        parcel.writeString(imeAutora);
        parcel.writeString(kategorija);
        parcel.writeInt(slika1);
        parcel.writeByte((byte) (boja ? 1 : 0));
    }
}
