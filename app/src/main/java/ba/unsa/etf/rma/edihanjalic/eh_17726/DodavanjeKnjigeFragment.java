package ba.unsa.etf.rma.edihanjalic.eh_17726;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.content.Context;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static ba.unsa.etf.rma.edihanjalic.eh_17726.DodavanjeKnjigeAkt.knjige;
import static ba.unsa.etf.rma.edihanjalic.eh_17726.KategorijeAkt.kategorije;

/**
 * Created by EH14 on 14-Apr-18.
 */

public class DodavanjeKnjigeFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dodavanjeknjigefragment,container,false);

        final Spinner sKategorijaKnjige = (Spinner)v.findViewById(R.id.sKategorijaKnjige);

        ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, kategorije);
        sKategorijaKnjige.setAdapter(adapter);
        Button dUpisiKnjigu = (Button)v.findViewById(R.id.dUpisiKnjigu);
        final EditText imeAutora = (EditText)v.findViewById(R.id.imeAutora);
        final EditText nazivKnjige = (EditText)v.findViewById(R.id.nazivKnjige);
        Button dPonisti = (Button)v.findViewById(R.id.dPonisti);

        dPonisti.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                ListeFragment lf = new ListeFragment();
                FragmentManager fm = getFragmentManager();
                Bundle b = new Bundle();
                b.putParcelableArrayList("knjiga1", knjige);
                lf.setArguments(b);
                fm.beginTransaction().replace(R.id.mjestoF1,lf).addToBackStack(null).commit();
            }
        });

        dUpisiKnjigu.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Knjiga temp = new Knjiga();
                temp.setNazivKnjige(nazivKnjige.getText().toString());
                temp.setImeAutora(imeAutora.getText().toString());
                temp.setKategorija(sKategorijaKnjige.getSelectedItem().toString());
                knjige.add(temp);
                ListeFragment lf = new ListeFragment();
                FragmentManager fm = getFragmentManager();
                Bundle b = new Bundle();
                b.putParcelableArrayList("knjiga1", knjige);
                lf.setArguments(b);
                fm.beginTransaction().replace(R.id.mjestoF1,lf).addToBackStack(null).commit();
            }
        });

      /*  Button dNadjiSliku = (Button)v.findViewById(R.id.dNadjiSliku);
        dNadjiSliku.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                if(intent.resolveActivity(getPackageManager()) != null){
                    startActivityForResult(intent,RESULT_LOAD_IMAGE);
                }
            }
        });

*/
        return v;
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor = getActivity().getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    public void onActivityResult(int requestcode, int resultcode, Intent data){
        if(resultcode == RESULT_CANCELED){}
        if(resultcode == RESULT_OK){
            EditText nazivKnjige = (EditText) getActivity().findViewById(R.id.nazivKnjige);
            Uri odabranaSlika = data.getData();
            try{
                FileOutputStream os;
                os = getActivity().openFileOutput(nazivKnjige.getText().toString(), Context.MODE_PRIVATE);
                try{
                    getBitmapFromUri(data.getData()).compress(Bitmap.CompressFormat.JPEG, 90, os);
                    os.close();
                } catch (IOException exxx){
                    Logger.getLogger(DodavanjeKnjigeAkt.class.getName()).log(Level.SEVERE, null, exxx);
                }
                ImageView slika = (ImageView)getActivity().findViewById(R.id.eNaslovna);
                slika.setImageBitmap(BitmapFactory.decodeStream(getActivity().openFileInput(nazivKnjige.getText().toString())));
            } catch (FileNotFoundException exx){
                Logger.getLogger(DodavanjeKnjigeAkt.class.getName()).log(Level.SEVERE, null, exx);
            }
        }

    }
}
